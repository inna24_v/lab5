#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include "list.h"
#include "FileWorks.h"


void foreach(LinkedList* l, void (*func)(int)){
    LinkedlistElement* element = l->firstElement;
	int i;
    for (i = 0; i < l->length; i++){
        func(element->value);
        element = element->NextElement;
    }
}

void printer(int i){
    printf("%d ", i);
}

void printerln(int i){
    printf("%d\n", i);
}

LinkedList* map(LinkedList* l, int (*func)(int)){
    LinkedList* newList = list_create_empty(0);
    LinkedlistElement* lle = l->firstElement;
	int i;
    for (i=l->length; i > 0; i--){
        list_add_back(func(lle->value),newList);
        lle = lle->NextElement;
    }
    return newList;
}

int square(int x){
    return x*x;
}

int cube(int x){
    return x*x*x;
}


int foldl(int a,int (*func)(int, int), LinkedList* l){
    LinkedlistElement* element = l->firstElement;
	int i;
    for (i = 0; i < l->length; i++){
        a = func(element->value, a);
        element = element->NextElement;
    }
    return a;
}

int sum(int x, int a){
    return a + x;
}

int minElem(int x, int a){
    if (x < a) return x;
    else return a;
}

int maxElem(int x, int a){
    if (x > a) return x;
    else return a;

}

void map_mut(LinkedList* l, int (*func)(int)){
LinkedlistElement* lle = l->firstElement;
int i;
    for (i=l->length; i > 0 ; i--){
        lle->value = func(lle->value);
        lle = lle->NextElement;
    }
    return;
}

int abs(int x){
    if (x < 0) return -x;
    else return x;
}

LinkedList* iterate(int s, int length, int (*func)(int)){
    LinkedList* l = list_create_empty(0);
	int i;
    for (i = 0; i<length; i++){
        list_add_back(s,l);
        s = func(s);
    }
    return l;
}

int mul(int x){
     return 2*x;
}

int main(int argc, char** argv) {
	int val;
	LinkedList* list_link = NULL;
	LinkedList** list_link1 = NULL;
	LinkedList* modif_list = NULL;
	char* filename = "file1";
	printf("Enter the value: ");
	scanf("%d", &val) ;
	list_link = list_create(val);
	modif_list = list_create_empty(0);
	while (scanf("%d", &val) == 1 ) {
        list_add_back(val, list_link);
    }
	list_link1 = &list_link;
	
	printf("List: ");
	foreach (list_link, &printer);
	printf("\n");
	
	printf("List: ");
	foreach (list_link, &printerln);
	printf("\n");

	printf("Squares:\n");
	print_list(modif_list = map(list_link, &square));
	list_free(modif_list);
	printf("\n");
	
	printf("Cubes:\n");
	print_list(modif_list = map(list_link, &cube));
	list_free(modif_list);
	printf("\n");

	printf("Foldl:\n");
	printf("Sum = %d\n", foldl(0, &sum, list_link));
	printf("min = %d\n", foldl(0, &minElem, list_link));
	printf("max = %d\n", foldl(0, &maxElem, list_link));
	printf("\n");
	
	printf("map_mut(abs): ");
	map_mut(list_link, &abs);
	foreach(list_link, &printer);
	printf("\n");

	printf("Iterations\n");
	print_list(iterate(1, 10, &mul));
	printf("\n");
	
	printf("Save: %d\n", save(list_link, filename));
	printf("Load: %d\n",load(list_link1, filename));
	
	list_free(list_link);
	return 0;

}
