#include <stdbool.h>
bool save(LinkedList* lst, const char* filename);
bool load(LinkedList** lst, const char* filename);
bool serialize(LinkedList* lst, const char* filename);
bool deserialize(LinkedList** lst, const char* filename);