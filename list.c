#include <stdio.h>
#include <stdlib.h>
#include "list.h"

LinkedList* list_create(int value){
    LinkedList* ll = (LinkedList*)malloc(sizeof(LinkedList));
    ll->firstElement = (LinkedlistElement*) malloc(sizeof(LinkedlistElement));
    ll->length = 1;
    ll->firstElement->value = value;
    ll->firstElement->NextElement = NULL;
    return ll;
}

LinkedList* list_create_empty(int length){
    LinkedList* ll = (LinkedList*)malloc(sizeof(LinkedList));
    ll->length = length;
    ll->firstElement = NULL;
    return ll;
}

void list_add_front(int value, LinkedList *l){
	LinkedlistElement *newNextElement;
    if (l->length == 0){
        l->firstElement = (LinkedlistElement*)malloc(sizeof(LinkedlistElement));
        l->firstElement->NextElement = NULL;
        l->firstElement->value = value;
        l->length++;
        return;
    }
    newNextElement = l->firstElement; 
    l->firstElement = (LinkedlistElement*)malloc(sizeof(LinkedlistElement));
    l->firstElement->value = value;
    l->firstElement->NextElement = newNextElement;
    if (l->length == 1){
        l->lastElement = newNextElement;
    }
    l->length++;
}

void list_add_back(int value, LinkedList *l){
	LinkedlistElement* newLast;
	LinkedlistElement* newLastElement;
    if (l->length==0){
        l->firstElement = malloc(sizeof(LinkedlistElement));
        l->firstElement->value = value;
        l->firstElement->NextElement = NULL;
        l->length++;
        return;
    }

    if (l->length ==1){
        newLast = (LinkedlistElement* ) malloc(sizeof(LinkedlistElement));
        newLast->value = value;
        l->lastElement = newLast;
        l->firstElement->NextElement = newLast;
        l->length++;
        return;
    }

    newLastElement =(LinkedlistElement*) malloc(sizeof(LinkedlistElement));
    newLastElement->NextElement = NULL;
    newLastElement->value = value;
    l->lastElement->NextElement = newLastElement;
    l->lastElement = newLastElement;
    l->length++;
}


int list_get(int index, LinkedList* l){
	int i;
	LinkedlistElement* element = l->firstElement;
    if (index >= l->length) return 0;
    if (l->length == 1) return l->firstElement->value;
    for (i = 0; i < index; i++)
        element = element->NextElement;
    return element->value;
}

void list_free(LinkedList* l){
    LinkedlistElement* element = l->firstElement;
    LinkedlistElement* nextElement;
	int i;
    for (i = 0; i < l->length; i++){
        nextElement = element->NextElement;
        free(element);
        element = nextElement;
    }
    free(l);
}

int list_length(LinkedList* l){
    return l->length;
}

LinkedlistElement* list_LinkedlistElement_at(int index, LinkedList* l){
	int i;
	LinkedlistElement* element = l->firstElement;
    if(index > l->length - 1) return NULL;
    for (i = 1; i < index; i++){
        element = element->NextElement;
    }
    return element;
}

long list_sum(LinkedList* l){
	LinkedlistElement* element = l->firstElement;
    int sum = element->value;
	int i;
    if (l->length == 0) return 0;
    for (i = 1; i < l->length; i++){
        element = element->NextElement;
        sum += element->value;
    }
    return sum;
}

void print_list(LinkedList* l){
	int i;
	printf("List:");
	for(i=0; i<list_length(l); i++)
		printf(" %d", list_get(i, l));
	printf("\n");
}