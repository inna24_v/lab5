#include <stdbool.h>
#include <stdio.h>
#include "list.h"

bool save(LinkedList* lst, const char* filename){
    char* mode = "w";
	FILE* f = fopen(filename, mode);
    LinkedlistElement* elem = lst->firstElement;
    int b =1, i;
	if (f == NULL) {
        return false;
    }

    for (i = 0; i < lst->length; ++i) {
        b = fprintf(f, "%d\n", elem->value);
        elem = elem->NextElement;
        if (b<0) return false;
    }
    fclose(f);
    return true;
}

bool load(LinkedList** lst, const char* filename){
    
    char* mode = "r";
	FILE* f = fopen(filename, mode);
    LinkedList* l = list_create_empty(0);
    int val;
	if (f == NULL) {
        return false;
    }

    while (fscanf(f, "%d", &val) != 1) {
        list_add_back(val, l);
    }
    *lst = l;
    if (ferror(f)) {
        return false;
    }
    return !fclose(f);
}

bool serialize(LinkedList* lst, const char* filename){
    char* mode = "wb";   
    FILE* f = fopen(filename, mode);
    LinkedlistElement* elem = lst->firstElement;
    int b =1, i;
    for (i = 0; i < lst->length; ++i) {
        b = fprintf(f, "%d\n", elem->value);
        elem = elem->NextElement;
        if (b<0) return false;
    }
    fclose(f);
    return true;
}

bool deserialize(LinkedList** lst, const char* filename){
    char* mode = "rb";
	FILE* f = fopen(filename, mode);
    int a = 1;
    LinkedList* l = list_create_empty(0);
    int val;
    a = fscanf(f,"%d", &val);
    while (a>0){
        list_add_back(val, l);
        a = fscanf(f,"%d", &val);
    }
    *lst = l;
    fclose(f);
    if (a<0) return false;
    return true;
}