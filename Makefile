all: main

main: main.c list.c list.h FileWorks.c
	gcc -o main -ansi -pedantic -Wall -Werror main.c list.c FileWorks.c
	
clean:
	rm -f main *.o