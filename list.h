#pragma once
typedef struct LinkedlistElement LinkedlistElement;
typedef struct LinkedList LinkedList;

struct LinkedlistElement{
    int value;
    LinkedlistElement *NextElement;
};


struct LinkedList{
    int length;
    LinkedlistElement* firstElement;
    LinkedlistElement* lastElement;
};

LinkedList* list_create_empty(int length);
LinkedList* list_create(int value);
void list_add_front(int value, LinkedList *l);
void list_add_back(int value, LinkedList *l);
int list_get(int index, LinkedList* l);
void list_free(LinkedList* l);
int list_length(LinkedList* l);
LinkedlistElement* list_LinkedlistElement_at(int index, LinkedList* l);
long list_sum(LinkedList* l);
void print_list(LinkedList* l);
